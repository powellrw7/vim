" execute pathogen#infect()
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Vundle managed plugins!
Plugin 'jpo/vim-railscasts-theme'
Plugin 'pbrisbin/vim-colors-off'
Plugin 'mattpap/vim-owl-tools'
Plugin 'mxw/vim-jsx'
Plugin 'shime/vim-livedown'
Plugin 'fatih/vim-go'
Plugin 'elixir-lang/vim-elixir'
Plugin 'duythinht/inori'
Plugin 'marijnh/tern_for_vim'
Plugin 'terryma/vim-expand-region'
Plugin 'junegunn/goyo.vim'
Plugin 'chriskempson/base16-vim'
Plugin 'gosukiwi/vim-atom-dark'
Plugin 'gmarik/Vundle.vim'
Plugin 'fs111/pydoc.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'SirVer/ultisnips'
Plugin 'bling/vim-airline'
Plugin 'tpope/vim-fugitive'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-notes'
Plugin 'honza/vim-snippets'
Plugin 'benmills/vimux'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'Valloric/YouCompleteMe'
Plugin 'FredKSchott/CoVim'
Plugin 'wikitopian/hardmode'
Plugin 'freitass/todo.txt-vim'
Plugin 'majutsushi/tagbar'
Plugin 'sjl/gundo.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'mikewest/vimroom'
Plugin 'altercation/vim-colors-solarized'
Plugin 'farseer90718/vim-taskwarrior'
Plugin 'owickstrom/vim-colors-paramount'

" Don't forget to ./installer.sh --clang-completer
call vundle#end()
" End of vundle stuff

" The most helpful thing in the world!
let mapleader="\<Space>"

" General
set nocompatible
set hidden
set incsearch                       " show search matches as you type
set nowrap                          " don't wrap lines
set backspace=indent,eol,start      " allow backspacing over everything in insert mode
set autoindent                      " always set autoindenting on
set copyindent                      " copy the previous indentation on autoindenting
set number                          " always show line numbers
set shiftwidth=4                    " number of spaces to use for autoindenting
set tabstop=4                       " a tab is four spaces
set expandtab						" Dunno what this one does
set shiftround                      " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch                       " set show matching parenthesis
set ignorecase                      " ignore case when searching
set smartcase                       " ignore case if search pattern is all lowercase,
set smarttab                        " insert tabs on the start of a line according to
set hlsearch                        " highlight search terms
set incsearch                       " show search matches as you type
set history=1000                    " remember more commands and search history
set undolevels=1000                 " use many muchos levels of undo
set title                           " change the terminal's title
set nobackup
set noswapfile
set wildignore=*.swp,*.bak,*.pyc,*.class
set clipboard+=unnamed
set completeopt=menuone,longest,preview
set foldmethod=indent
set foldlevel=99

syntax on
filetype plugin indent on

" Let .md files be markdown and not modela
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown

" Syntax highlighting for OWL!
au BufRead,BufNewFile *.owl set filetype=owl
" Fix crontab edit issues
au BufEnter /private/tmp/crontab.* setl backupcopy=yes

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>lv :so $MYVIMRC<CR>

" Enable region expansion shortcut
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" eclim autocomplete
let g:EclimCompletionMethod = 'omnifunc'

" JSX
let g:jsx_ext_required = 0 " Allow JSX in normal JS files

" Colors / Layout
colorscheme off
set background=dark

set laststatus=2

" Encourage Vimdom
nnoremap ; :
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
nnoremap j gj
nnoremap k gk

" Syntastic
let g:syntastic_php_checkers = ['php']
let g:syntastic_javascript_checkers = ['eslint']

" Airline settings
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '

" Tagbar 
nmap <silent> <leader>tb :TagbarToggle<cr>

let g:tagbar_type_elixir = {
    \ 'ctagstype' : 'elixir',
    \ 'kinds' : [
        \ 'f:functions',
        \ 'functions:functions',
        \ 'c:callbacks',
        \ 'd:delegates',
        \ 'e:exceptions',
        \ 'i:implementations',
        \ 'a:macros',
        \ 'o:operators',
        \ 'm:modules',
        \ 'p:protocols',
        \ 'r:records'
    \ ]
\ }

" Gundo
nmap <silent> <leader>gd :GundoToggle<cr>

" File Browsing
nmap <silent> <leader>le :Lexplore<cr>
nmap <silent> <leader>ln :lnext<cr>
nmap <silent> <leader>lp :lprev<cr>
nmap <silent> <leader>lo :lopen<cr>
nmap <silent> <leader>nt :NERDTreeToggle<cr>
let g:netrw_winsize = 30 
let g:NERDTreeChDirMode = 2

" Vimux
map <Leader>vp :VimuxPromptCommand<CR>
map <Leader>vl :VimuxRunLastCommand<CR>
map <Leader>vi :VimuxInspectRunner<CR>
map <Leader>vq :VimuxCloseRunner<CR>
map <Leader>vx :VimuxInterruptRunner<CR>
map <Leader>vz :call VimuxZoomRunner()<CR>map <Leader>ns :NoteFromSelectedText<CR>

" YouCompleteMe + Utilitsnips
let g:ycm_key_list_select_completion=['<C-j>']
let g:ycm_key_list_previous_completion=['<C-k>']
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<C-b>"
let g:UltiSnipsJumpBackwardTrigger="<C-z>"

" No more ldkfjlsdfjdlkjf to clear search
cmap w!! w !sudo tee % >/dev/null

" Spellcheck
map <leader>ss :setlocal spell!<cr>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=

" Easy splits and such
map <leader>sv :vsplit<cr>
map <leader>sh :split<cr>

" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_


" CrtlP maddness
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site)$',
  \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg)$',
\}

let g:ctrlp_working_path_mode = 'r'
nmap <leader>T :enew<cr>
nmap <leader>bq :bp <BAR> bd #<cr>
nmap <leader>p :CtrlP<cr>
nmap <leader>bb :CtrlPBuffer<cr>
nmap <leader>bm :CtrlPMixed<cr>
nmap <leader>bs :CtrlPMRU<cr>

" Buffer Management
map <leader>1 :b1<cr>
map <leader>2 :b2<cr>
map <leader>3 :b3<cr>
map <leader>4 :b4<cr>
map <leader>5 :b5<cr>
map <leader>6 :b6<cr>
map <leader>7 :b7<cr>
map <leader>8 :b8<cr>
map <leader>9 :b9<cr>
map <leader>bn :bn<cr>
map <leader>bp :bp<cr>

" Tab Management
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove
map <leader>tf :tabNext<cr>
map <leader>tp :tabprevious<cr>
map <leader>t1 :tabfir<cr>
map <leader>t2 :tabn 2<cr>
map <leader>t3 :tabn 3<cr>
map <leader>t4 :tabn 4<cr>
map <leader>t5 :tabn 5<cr>
map <leader>t6 :tabn 6<cr>
map <leader>t7 :tabn 7<cr>
map <leader>t8 :tabn 8<cr>
map <leader>t9 :tabn 9<cr>
map <leader>t0 :tabl<cr>
